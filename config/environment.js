'use strict';

module.exports = function (environment) {
    let ENV = {
        modulePrefix: 'cloud-address',
        environment,
        firebase: {
            apiKey: 'AIzaSyC49FrwQY1AObF_2QrKlULo4RBLN69ioXQ',
            authDomain: 'cloud-address-me.firebaseapp.com',
            databaseURL: 'https://cloud-address-me.firebaseio.com',
            projectId: 'cloud-address-me',
            storageBucket: 'cloud-address-me.appspot.com',
            messagingSenderId: '64535035330',
            appId: '1:64535035330:web:6e9e1a6df82b8aa4'
        },
        rootURL: '/',
        locationType: 'hash',
        EmberENV: {
            FEATURES: {
                // Here you can enable experimental features on an ember canary build
                // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
            },
            EXTEND_PROTOTYPES: {
                // Prevent Ember Data from overriding Date.parse.
                Date: false
            }
        },

        APP: {
            // Here you can pass flags/options to your application instance
            // when it is created
        }
    };

    if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    }

    if (environment === 'test') {
    // Testem prefers this...
        ENV.locationType = 'none';

        // keep test console output quieter
        ENV.APP.LOG_ACTIVE_GENERATION = false;
        ENV.APP.LOG_VIEW_LOOKUPS = false;

        ENV.APP.rootElement = '#ember-testing';
        ENV.APP.autoboot = false;
    }

    if (environment === 'production') {
        ENV['ember-facebook-pixel'] = {
            id: '618184518378229'
        };
        ENV.PROD = true;
    // here you can enable a production-specific feature
    }

    return ENV;
};
