import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | upgrade', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:upgrade');
    assert.ok(route);
  });
});
