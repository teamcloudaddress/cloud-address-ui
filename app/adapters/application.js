import RealtimeDatabaseAdapter from 'emberfire/adapters/realtime-database';

export default RealtimeDatabaseAdapter.extend({
    // configuration goes here
    shouldReloadAll(store, snapshotArray) {
        return true;
    },
    shouldBackgroundReloadAll(store, snapshotArray) {
        return true;
    }
});
