import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from "@ember/object";

export default Controller.extend({
    navigator: service(),
    contact: service(),
    toasts: service(),

    name: computed.alias('model.name'),
    address: computed.alias('model.address'),
    address2: computed.alias('model.address2'),
    city: computed.alias('model.city'),
    state: computed.alias('model.state'),
    country: computed.alias('model.country'),
    zip: computed.alias('model.zip'),
    email: computed.alias('model.email'),
    phone: computed.alias('model.phone'),
    notes: computed.alias('model.notes'),

    // show() {
    //     this.set("showSaveConfirmation", true);

    //     setTimeout(() => {
    //         this.set("showSaveConfirmation", false);
    //         this.set('midProcess', false);
    //     }, 100);
    // },
    // loop() {
    //     setTimeout(() => {
    //         this.lowerCounter();

    //         if (this.get("counter") < 0) {
    //             this.set('midProcess', false);
    //             this.show();
    //         } else {
    //             this.loop();
    //         }
    //     }, 100);
    // },
    // lowerCounter() {
    //     this.set("counter", this.get("counter") - 1);
    // },

    actions: {
        onBack() {
            this.transitionToRoute("index");
        },
        // showConfirmation() {
        //     this.set('counter', 10);
        //     if (!this.get('midProcess')) {
        //         this.set('midProcess', true);

        //         this.loop();
        //     }
        // },
        updateContact() {

            let contactId = this.get('model.id');
            let fieldsObj = {
                name: this.get('name'),
                address: this.get('address'),
                address2: this.get('address2'),
                city: this.get('city'),
                state: this.get('state'),
                country: this.get('country'),
                zip: this.get('zip'),
                email: this.get('email'),
                phone: this.get('phone'),
                notes: this.get('notes')
            };

            this.get('contact').update(this.store, contactId, fieldsObj);

            this.get("toasts").successfullySaved();
        }
    }
});
