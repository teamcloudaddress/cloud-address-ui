import Controller from '@ember/controller';
import {
  inject as service
} from '@ember/service';
import {
  computed
} from "@ember/object";
import _ from 'lodash';

export default Controller.extend({
  analytics: service(),
  list: service(),
  session: service(),
  state: service(),
  router: service(),

  user: computed.alias("session.data.authenticated.user"),

  notOnPlainView: computed('router.location.location.hash', function () {
    let views = ['terms', 'welcome'];
    let onPlainView = false;
    const HASH = this.get('router.location.location.hash');

    _.each(views, view => {
      if (_.includes(HASH, view)) {
        onPlainView = true;
      }
    })

    return !onPlainView;
  }),

  actions: {
    sendHome() {
      this.send("sendToIndexRoute");
      this.set('state.selectedList', null);
    },
    sendToIndexRoute() {
      this.transitionToRoute("index");
    },
    logout() {
      this.transitionToRoute("welcome");
      this.get('session').invalidate();
    },
    deleteList(listId) {
      this.get('list').delete(this.store, listId);
    },
    goToWelcomePage() {
      this.transitionToRoute("welcome");
    }
  }
});
