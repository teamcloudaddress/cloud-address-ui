/* eslint-disable ember/no-global-jquery */
import Controller from '@ember/controller';
import firebase from 'firebase/app';
import {
  inject as service
} from '@ember/service';
import {
  computed
} from "@ember/object";

export default Controller.extend({
  firebaseApp: service(),
  analytics: service(),
  state: service(),

  params: computed.alias('state.params'),


  contactFocus: computed('params.ver', function() {
      return this.get('params.ver') === 'c';
  }),
  showTopSignInWrapper: false,

  showSignInTop: false,
  signInEmailTop: null,
  signInPasswordTop: null,
  signUpEmailTop: null,
  signUpPasswordTop: null,

  showSignInBottom: false,
  signInEmailBottom: null,
  signInPasswordBottom: null,
  signUpEmailBottom: null,
  signUpPasswordBottom: null,

  actions: {
    showSignInWrapper() {
      $("#show-sign-in-wrapper-button").hide();
      $(".sign-in-wrapper.top").slideDown(300);
      this.set('showTopSignInWrapper', true);
    },
    hideSignInWrapper() {
      $(".sign-in-wrapper.top").slideUp(300);
      setTimeout(() => {
        $("#show-sign-in-wrapper-button").fadeIn();
        this.set('showTopSignInWrapper', false);
      }, 300);
    },
    viewSignInTabTop() {
      this.set('showSignInTop', true);
    },
    viewSignUpTabTop() {
      this.set('showSignInTop', false);
    },

    viewSignInTabBottom() {
      this.set('showSignInBottom', true);
    },
    viewSignUpTabBottom() {
      this.set('showSignInBottom', false);
    },

    // TOP
    async signUpWithEmailAndPasswordTop() {
      const email = this.get('signUpEmailTop');
      const password = this.get('signUpPasswordTop');
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(() => {

          this.get('analytics').trackEvent({
            pixelID: "Successfully Sign Up",
            action: "Sign Up - Using Email and Password"
          });

          this.set('signUpEmailTop', null);
          this.set('signUpPasswordTop', null);

          this.transitionToRoute("index");

          // HACK!
          // TODO: FIX THIS!
          setTimeout(() => {
            location.reload();
          }, 100);
        })
        .catch(err => {
          this.get('analytics').trackEvent({
            action: "ERROR",
            label: err
          })
        });
    },
    async signInWithEmailAndPasswordTop() {
      const email = this.get('signInEmailTop');
      const password = this.get('signInPasswordTop');
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(() => {

          this.get('analytics').trackEvent({
            pixelID: "Successfully Sign In",
            action: "Sign In - Using Email and Password"
          });

          this.set('signInEmailTop', null);
          this.set('signInPasswordTop', null);

          this.transitionToRoute("index");

          // HACK!
          // TODO: FIX THIS!
          setTimeout(() => {
            location.reload();
          }, 100);
        })
        .catch(err => {
          this.get('analytics').trackEvent({
            action: "ERROR",
            label: err
          })
        });
    },

    // BOTTOM
    async signUpWithEmailAndPasswordBottom() {
      const email = this.get('signUpEmailBottom');
      const password = this.get('signUpPasswordBottom');
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(() => {

          this.get('analytics').trackEvent({
            pixelID: "Successfully Sign Up",
            action: "Sign Up - Using Email and Password"
          });

          this.set('signUpEmailBottom', null);
          this.set('signUpPasswordBottom', null);

          this.transitionToRoute("index");

          // HACK!
          // TODO: FIX THIS!
          setTimeout(() => {
            location.reload();
          }, 100);
        })
        .catch(err => {
          this.get('analytics').trackEvent({
            action: "ERROR",
            label: err
          })
        });
    },
    async signInWithEmailAndPasswordBottom() {
      const email = this.get('signInEmailBottom');
      const password = this.get('signInPasswordBottom');
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(() => {

          this.get('analytics').trackEvent({
            pixelID: "Successfully Sign In",
            action: "Sign In - Using Email and Password"
          });

          this.set('signInEmailBottom', null);
          this.set('signInPasswordBottom', null);

          this.transitionToRoute("index");

          // HACK!
          // TODO: FIX THIS!
          setTimeout(() => {
            location.reload();
          }, 100);
        })
        .catch(err => {
          this.get('analytics').trackEvent({
            action: "ERROR",
            label: err
          })
        });
    },

    async loginWithGoogle() {
      const provider = new firebase.auth.GoogleAuthProvider();
      const auth = await this.get('firebaseApp').auth();
      return auth.signInWithPopup(provider).then(() => {

        this.get('analytics').trackEvent({
          pixelID: "Successfully Sign In",
          action: "Sign In - Using Google"
        });

        this.transitionToRoute("index");

        // HACK!
        // TODO: FIX THIS!
        setTimeout(() => {
          location.reload();
        }, 100);
      });
    },
    async loginWithFb() {
      const provider = new firebase.auth.FacebookAuthProvider();
      const auth = await this.get('firebaseApp').auth();
      return auth.signInWithPopup(provider).then(() => {

        this.get('analytics').trackEvent({
          pixelID: "Successfully Sign In",
          action: "Sign In - Using Facebook"
        });

        this.transitionToRoute("index");

        // HACK!
        // TODO: FIX THIS!
        setTimeout(() => {
          location.reload();
        }, 100);
      });
    },
  }
});
