import Controller from '@ember/controller';

export default Controller.extend({
    actions: {
        stayBasic() {
            this.transitionToRoute("index");
        }
    }
});
