/* eslint-disable ember/no-global-jquery */
import Controller from '@ember/controller';
import {
  inject as service
} from '@ember/service';
import {
  computed
} from "@ember/object";
import _ from "lodash";

export default Controller.extend({
  analytics: service(),
  contact: service(),
  list: service(),
  state: service(),
  toasts: service(),

  selectedList: computed.alias("state.selectedList"),
  onList: computed("selectedList.name", "header", function () {
    return this.get('selectedList');
  }),

  activeTab: "list",
  contactPendingDeletion: null,
  filterValue: null,
  contactCount: computed('contacts', 'unfilteredCount', function () {
    return {
      all: this.get('allContacts').length,
      filtered: this.get('contacts').length,
    };
  }),
  contactsNotInList: computed("model.allContacts.[]", "contact.all.[]", "state.selectedList.[]", "listContactRels", function () {
    let returnContacts;

    if (this.get("contact.all")) {
      returnContacts = this.get("contact.all");
    } else {
      returnContacts = this.get("model.allContacts");
    }

    if (this.get('state.selectedList')) {
      returnContacts = _.filter(returnContacts.toArray(), contact => {
        var valid = true;

        _.each(this.get('listContactRels').toArray(), rel => {
          if (rel.contactId === contact.id && rel.listId === this.get('state.selectedList.id')) {
            valid = false;
          }
        });

        return valid;
      })
    }

    return returnContacts.toArray();
  }),
  allContacts: computed("model.allContacts.[]", "contact.all.[]", "state.selectedList.[]", "listContactRels", function () {
    let returnContacts;

    if (this.get("contact.all")) {
      returnContacts = this.get("contact.all");
    } else {
      returnContacts = this.get("model.allContacts");
    }

    if (this.get('state.selectedList')) {
      returnContacts = _.filter(returnContacts.toArray(), contact => {
        var valid = false;

        _.each(this.get('listContactRels').toArray(), rel => {
          if (rel.contactId === contact.id && rel.listId === this.get('state.selectedList.id')) {
            valid = true;
          }
        });

        return valid;
      })
    }

    return returnContacts.toArray();
  }),
  contacts: computed("allContacts", "filterValue", function () {
    let filterValue = _.toLower(this.get('filterValue')).trim();
    let returnContacts = this.get('allContacts');


    if (filterValue) {

      return _.filter(returnContacts, contact => {

        let fields = ['name', 'address', 'city', 'state', 'zip', 'email', 'phone', 'notes'];
        let found = false;

        _.each(fields, (field) => {
          if (_.includes(_.toLower(contact[field]), _.toLower(filterValue))) {
            found = true
          }
        });

        return found;
      });
    } else {
      return returnContacts;
    }

  }),
  listContactRels: computed("model.listContactRels.[]", "list.allRels.[]", function () {
    if (this.get("list.allRels")) {
      return this.get("list.allRels").toArray();
    } else {
      return this.get("model").listContactRels;
    }
  }),
  lists: computed("model.lists.[]", "list.all.[]", function () {
    if (this.get("list.all")) {
      return this.get("list.all");
    } else {
      return this.get("model.lists");
    }
  }),

  actions: {
    selectTab(tab) {
      this.set('activeTab', tab);
    },
    // IMPORT CONTACTS
    showImportContactsModal() {
      $("#import-contacts-modal-gauze").fadeIn();
    },
    hideImportContactsModal() {
      $("#import-contacts-modal-gauze").fadeOut();
    },
    importContacts(contactsArr) {
      this.send('hideImportContactsModal')
      let listId = this.get('state.selectedList') ? this.get('state.selectedList.id') : false;
      this.get("contact").createMultiple(this.store, contactsArr, listId);

      this.get('analytics').trackEvent({
        action: "Contact Management",
        label: "Import",
        pixelID: "Import Contacts"
      });
      this.get('toasts').success("List succcessfully imported.");
    },

    // CREATE CONTACT
    createContact() {
      let listId = this.get('state.selectedList') ? this.get('state.selectedList.id') : false;

      this.get("contact").create(this.store, listId, null, (contactId) => {
        this.transitionToRoute('contacts', contactId);
      });

      this.get('toasts').success("Contact succcessfully created.");
    },

    // ADD CONTACTS
    showAddContactsModal() {
      $("#modal-gauze").fadeIn();
    },
    addContacts(selectedContacts) {
      this.get('list').addContacts(this.store, this.get('selectedList.id'), selectedContacts);
      $("#modal-gauze").fadeOut();
      this.get("toasts").success('Contacts successfully added.');
    },

    // DELETE CONTACT
    showConfirmDeleteModal(contact) {
      $("#confirm-contact-delete-gauze").fadeIn();
      this.set("contactPendingDeletion", contact);
    },
    hideConfirmDeleteModal() {
      $("#confirm-contact-delete-gauze").fadeOut(100);
      this.set("contactPendingDeletion", null);
    },
    deleteContact() {
      this.get("contact").delete(this.store, this.get('contactPendingDeletion.id'));
      this.send("hideConfirmDeleteModal");
    },

    // REMOVE CONTACT FROM LIST
    removeContactFromList(rel) {
      this.get('list').removeContact(this.store, rel.id);
    },
    removeContactFromThisList(rel) {
      this.get('list').removeContact(this.store, rel.id);
    },

    // ADD CONTACT TO LIST
    addContactToList(listId, contactId) {
      this.get('list').addContact(this.store, listId, contactId);
    },

    // VIEW CONTACT
    viewContact(contactId) {
      this.transitionToRoute('contacts', contactId);
    },

    // ACTIONS
    showDropdown(id) {
      if ($(`#dropdown-${id}`).css('display') === "none") {
        $(`#dropdown-${id}`).css('display', 'grid');
      }
    },
    hideDropdown(id) {
      $(`#dropdown-${id}`).css('display', 'none');
    },
    clearFilter() {
      this.set('filterValue', null);
    },
    exportList() {
      let contacts = _.map(this.get('contacts'), contact => {
        const {
          name,
          address,
          address2,
          city,
          state,
          country,
          zip,
          email,
          phone,
          notes
        } = contact;
        return {
          name,
          address,
          address2,
          city,
          state,
          country,
          zip,
          email,
          phone,
          notes
        };
      });

      var objectToCSVRow = function (dataObject) {
        var dataArray = new Array;
        for (var o in dataObject) {

          var innerValue = _.isNull(dataObject[o]) || _.isUndefined(dataObject[o]) ? '' : dataObject[o].toString();
          var result = innerValue.replace(/"/g, '""').replace(/,/g, "");
          result = '"' + result + '"';

          dataArray.push(result);
        }

        return dataArray.join(', ') + '\r\n';
      }

      var exportToCSV = function (arrayOfObjects) {

        if (!arrayOfObjects.length) {
          return;
        }

        var csvContent = "data:text/csv;charset=utf-8,";

        // headers
        csvContent += '"NAME", "ADDRESS", "ADDRESS 2", "CITY", "STATE", "ZIP", "EMAIL", "PHONE", "NOTES"\r\n';

        arrayOfObjects.forEach(function (item) {
          csvContent += objectToCSVRow(item);
        });

        let commaFree = csvContent.replace(/"/g, "");

        var encodedUri = encodeURI(commaFree);

        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "contacts.csv");
        document.body.appendChild(link); // Required for FF
        link.click();
        document.body.removeChild(link);
      }

      exportToCSV(contacts);

      this.get('analytics').trackEvent({
        action: "List Management",
        label: "Export",
        pixelID: "Export List"
      });

      this.get('toasts').success("List succcessfully exported.");

    }
  }
});
