import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function () {
  this.route('lists');
  this.route('contacts', { path: "/contacts/:contact_id" });
  this.route('upgrade');
  this.route('welcome');
  this.route('terms');
});

export default Router;
