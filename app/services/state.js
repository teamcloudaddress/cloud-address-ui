import Service from '@ember/service';
import {
  inject as service
} from '@ember/service';
import {
  computed
} from "@ember/object";
import objFromQueryParams from "../utils/obj-from-query-params";

export default Service.extend({
  router: service(),

  selectedList: null,

  params: computed('router.location.location.search', function () {
    return objFromQueryParams(this.get('router.location.location.search'));
  }),
});
