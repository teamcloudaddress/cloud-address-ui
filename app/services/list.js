import Service from '@ember/service';
import {
  inject as service
} from '@ember/service';
import {
  computed
} from "@ember/object";
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';
import _ from "lodash";

export default Service.extend(FindQuery, {
  // SERVICES
  analytics: service(),
  state: service(),
  session: service(),

  // COMPUTEDS
  user: computed.alias("session.data.authenticated.user"),

  // PROPERTIES
  all: null,
  allRels: null,

  // METHODS
  create(store) {
    this.get('analytics').trackEvent({
      action: "List Management",
      label: "Create",
      pixelID: "Create List"
    });

    // create new list
    let self = this;
    store.createRecord('list', {
        name: "New List",
        uid: this.get('user.uid'),
        color: "default"
      })
      .save()
      .then((newList) => {
        this.set('state.selectedList', newList);
        self.filterContains(store, 'list', {
          "uid": self.get('user.uid')
        }, function (allLists) {
          self.set('all', allLists);
        });
      });
  },
  rename(store, listId, newName) {
    this.get('analytics').trackEvent({
      action: "List Management",
      label: "Rename",
      pixelID: "Rename List"
    });

    store
      .findRecord('list', listId)
      .then(list => {
        list.set("name", newName);
        list.save();
      })
  },
  updateColor(store, listId, color) {
    this.get('analytics').trackEvent({
      action: "List Management",
      label: "Change Color",
      pixelID: "Change List Color"
    });

    store
      .findRecord('list', listId)
      .then(list => {
        list.set("color", color);
        list.save();
      })
  },
  delete(store, listId) {
    let self = this;

    this.get('analytics').trackEvent({
      action: "List Management",
      label: "Delete",
      pixelID: "Delete List"
    });

    this.set('state.selectedList', null);
    store
      .findRecord('list', listId)
      .then(list => {
        return list.destroyRecord();
      }).then(() => {
        // remove all the contact/list rels associated with this list
        self.forAllRelsForList(store, listId, listRel => {
          store
            .findRecord('listContactRel', listRel.id)
            .then(listRel => {
              return listRel.destroyRecord();
            })
        });

        self.filterContains(store, 'list', {
          "uid": self.get('user.uid')
        }, function (allLists) {
          self.set('all', allLists);
        });
      })
  },
  removeContact(store, relId) {
    let self = this;
    this.get('analytics').trackEvent({
      action: "List Management",
      label: "Remove Contact",
      pixelID: "Remove Contact from List"
    });

    store
      .findRecord('list-contact-rel', relId)
      .then(rel => {
        return rel.destroyRecord();
      })
      .then(() => {
        self.getAllRels(store);
      });
  },
  addContact(store, listId, contactId) {
    this.get('analytics').trackEvent({
      action: "List Management",
      label: "Add Contact",
      pixelID: "Add Contact to List"
    });

    let self = this;
    return store.createRecord('list-contact-rel', {
        contactId: contactId,
        listId: listId,
        uid: self.get('user.uid')
      })
      .save().then(() => {
        self.getAllRels(store);
      });

  },
  addContacts(store, listId, contactIdArr) {
    _.each(contactIdArr, contactId => this.addContact(store, listId, contactId))
  },
  forAllRelsForList(store, listId, callback) {
    let self = this;

    self.filterContains(store, 'list-contact-rel', {
      "listId": listId
    }, function (listRels) {
      _.each(listRels, listRel => {
        callback(listRel);
      });
    });
  },
  getAllRels(store) {
    let self = this;

    self.filterContains(store, 'list-contact-rel', {
      "uid": self.get('user.uid')
    }, function (allRels) {
      self.set('allRels', null);
      self.set('allRels', allRels);
    });
  }
});
