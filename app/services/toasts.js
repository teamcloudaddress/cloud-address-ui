import Service from '@ember/service';

export default Service.extend({
    showSaveSuccessToast: false,
    showMessageSuccessToast: false,

    counter: 10,
    midProcess: false,

    show() {
        this.set("showSaveSuccessToast", true);

        setTimeout(() => {
            this.set("showSaveSuccessToast", false);
            this.set('midProcess', false);
        }, 100);
    },
    loop() {
        setTimeout(() => {
            this.lowerCounter();

            if (this.get("counter") < 0) {
                this.set('midProcess', false);
                this.show();
            } else {
                this.loop();
            }
        }, 100);
    },
    lowerCounter() {
        this.set("counter", this.get("counter") - 1);
    },
    successfullySaved() {
        this.set('counter', 10);
        if (!this.get('midProcess')) {
            this.set('midProcess', true);

            this.loop();
        }

    },
    success(message) {
        this.set('showMessageSuccessToast', true);

        this.set('successMessage', message);

        setTimeout(() => {
            this.set('showMessageSuccessToast', false);
        }, 500);
    },
});
