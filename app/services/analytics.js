/* eslint-disable ember/no-global-jquery */
import Service from '@ember/service';
import {
  inject as service
} from '@ember/service';
import _ from 'lodash';
import envConfig from '../config/environment';

export default Service.extend({
  navigator: service(),
  state: service(),

  init() {
    const PATH = window.location.href;
    if (envConfig.PROD) {

      (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
          (i[r].q = i[r].q || []).push(arguments);
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
          m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m);
      })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-141567423-1', 'auto');
      ga('send', 'pageview', PATH);
    }
  },
  // For multiple types of events on a single page - like forms
  trackEventGroup(config) {

    const PATH = this.get('navigator.currentPath');
    let {
      name = 'Default Event Group Name',
        rules = {
          onPage: ''
        },
        events = []
    } = config;

    if (_.includes(PATH, rules.onPage)) {
      setTimeout(() => {

        if (envConfig.PROD && !_.includes(window.location.href, 'qa.sittersquare.com')) {
          _.each(events, (event) => {
            if (event.type) {
              $(event.element)[event.type](() => {
                ga('send', {
                  hitType: 'event',
                  eventCategory: name,
                  eventAction: event.name,
                  eventLabel: $(event.element).val()
                });
              });
            } else {
              ga('send', {
                hitType: 'event',
                eventCategory: name,
                eventAction: event.name,
                eventLabel: event.label
              });
            }
          });
        }
      }, 500);
    }
  },
  // For single events - like actions
  trackEvent(config) {

    // const PATH = this.get('navigator.currentPath');
    const PATH = this.get(window.location.href);
    let {
      category = PATH,
        action,
        label = '',
        pixelID
    } = config;

    if (envConfig.PROD) {
      ga('send', {
        hitType: 'event',
        eventCategory: category,
        eventAction: action,
        eventLabel: label
      });

      if (pixelID) {
        window.fbq('trackCustom', `CA - ${pixelID}`);
      }
    }
  }
});
