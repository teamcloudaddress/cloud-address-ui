import Service from '@ember/service';
import {
  inject as service
} from '@ember/service';
import {
  computed
} from "@ember/object";
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';
import uploadDataToJson from "../utils/upload-data-to-json";
import _ from "lodash";

export default Service.extend(FindQuery, {
  session: service(),
  analytics: service(),
  list: service(),
  user: computed.alias("session.data.authenticated.user"),

  all: null,

  // methods
  create(store, listId, seedData, callback) {
    let multiple = true;

    if (!seedData) {
      multiple = false;
      seedData = {};
    }

    let self = this;
    // defaults - unless given seed data
    let {
      name = "New Contact",
        address = "",
        city = "",
        state = "",
        zip = "",
        email = "",
        phone = "",
        notes = ""
    } = seedData;

    this.get('analytics').trackEvent({
      action: "Contact Management",
      label: "Create",
      pixelID: "Create Contact"
    });

    store.createRecord('contact', {
        lastSaved: moment.now(),
        name: name,
        address: address,
        city: city,
        state: state,
        zip: zip,
        email: email,
        phone: phone,
        notes: notes,
        uid: this.get('user.uid')
      })
      .save().then(newContact => {
        self.filterContains(store, 'contact', {
          "uid": self.get('user.uid')
        }, function (allContacts) {
          self.set('all', allContacts);
        });

        // if they are on a list
        if (listId) {
          this.get('list').addContact(store, listId, newContact.id);
        }

        if (!multiple) {
          callback(newContact.id);
        }
      });
  },
  createMultiple(store, contactsArr, listId) {
    // need arr of contacts
    let parsedContacts = uploadDataToJson(contactsArr);

    _.each(parsedContacts, contact => {

      this.create(store, listId, contact);
    });
  },
  update(store, contactId, fieldsObj) {
    this.get('analytics').trackEvent({
      action: "Contact Management",
      label: "Update",
      pixelID: "Update Contact"
    });

    store
      .findRecord("contact", contactId)
      .then(contact => {
        contact.set('lastSaved', moment.now());
        contact.set('name', fieldsObj.name);
        contact.set('address', fieldsObj.address);
        contact.set('city', fieldsObj.city);
        contact.set('state', fieldsObj.state);
        contact.set('zip', fieldsObj.zip);
        contact.set('email', fieldsObj.email);
        contact.set('phone', fieldsObj.phone);
        contact.set('notes', fieldsObj.notes);

        contact.save();
      });
  },
  delete(store, contactId) {
    let self = this;

    this.get('analytics').trackEvent({
      action: "Contact Management",
      label: "Delete",
      pixelID: "Delete Contact"
    });

    store
      .findRecord('contact', contactId)
      .then(contact => {
        contact.destroyRecord().then(() => {
          self.filterContains(store, 'contact', {
            "uid": self.get('user.uid')
          }, function (allContacts) {
            self.set('all', allContacts);
          });
        })
      });

    // store
    //     .findAll('list-contact-rel', contactId)
    //     .then(contact => {

    //         // contact.destroyRecord();
    //     });
    // deletes this contact - including all the references to it in the list_contact_rel collection
  }
});
