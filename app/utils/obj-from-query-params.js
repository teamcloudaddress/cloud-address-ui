import _ from "lodash";

export default function objFromQueryParams(queryStr) {
  let obj = {};
  let splitParams = queryStr
    .replace(/\?/gi, '') // take out the "=" sign
    .split('&'); // split it on the ampersand

  _.each(splitParams, paramPair => {
    let keyValPair = paramPair.split('=');
    obj[_.first(keyValPair)] = _.last(keyValPair);
  });

  return obj;
}
