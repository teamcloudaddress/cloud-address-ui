import _ from "lodash";

export default function uploadDataToJson(arr) {
  let returnArr = [];
  let indexKeys = {};
  let indexes = [
    "name",
    "address",
    "city",
    "state",
    "zip",
    "email",
    "phone",
    "notes"
  ];

  // determine indexes
  _.each(arr[0], row => {
    _.each(indexes, (index, i) => {
      if (_.includes(row.toLowerCase(), index.toLowerCase())) {
        indexKeys[index] = i;
      }
    })
  });


  _.each(arr, row => {
    if (row.length > 0 && hasMoreThanEmptySpaces(row) && !onHeader(row)) {
      returnArr.push({
        name: row[indexKeys.name].trim(),
        address: row[indexKeys.address].trim(),
        city: row[indexKeys.city].trim(),
        state: row[indexKeys.state].trim(),
        zip: row[indexKeys.zip].trim(),
        email: row[indexKeys.email].trim(),
        phone: row[indexKeys.phone].trim(),
        notes: row[indexKeys.notes].trim()
      });
    }
  });

  return returnArr;
}

function onHeader(row) {
  let onHeaderCount = 0;

  let indexes = [
    "name",
    "address",
    "city",
    "state",
    "zip",
    "email",
    "phone",
    "notes"
  ];

  _.each(row, col => {
    _.each(indexes, index => {
      if (col.toLowerCase() == index.toLowerCase()) {
        onHeaderCount += 1;
      }
    })
  })

  return onHeaderCount > 2;
}

function hasMoreThanEmptySpaces(row) {
  let valid = false;

  _.each(row, col => {
    if (col.trim().length > 0) {
      valid = true;
    }
  })

  return valid;
}