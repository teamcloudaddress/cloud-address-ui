import _ from "lodash";

export default function determineContactsCountInUpload(arr) {
  let count = 0;

  _.each(arr, row => {
    if (row.length > 1) {
      count += 1;
    }
  });

  return count - 1; // -1 because the top one is the header.
}
