import DS from 'ember-data';
const { Model, attr } = DS;

export default Model.extend({
    lastSaved: attr("string"),
    name: attr("string"),
    address: attr("string"),
    address2: attr("string"),
    city: attr("string"),
    state: attr("string"),
    country: attr("string"),
    zip: attr("string"),
    email: attr("string"),
    phone: attr("string"),
    notes: attr("string"),
    uid: attr("string")
});
