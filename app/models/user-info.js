import DS from 'ember-data';
const { Model, attr } = DS;

export default Model.extend({
    plan: attr("string"),
    uid: attr("string")
});
