import DS from 'ember-data';
const { Model, attr } = DS;

export default Model.extend({
    listId: attr('string'),
    contactId: attr('string'),
    uid: attr('string')
});
