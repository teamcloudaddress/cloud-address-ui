import Route from '@ember/routing/route';
import {
  inject as service
} from '@ember/service';
import {
  computed
} from "@ember/object";
// import RealtimeRouteMixin from 'emberfire/mixins/realtime-route';
import FindQuery from 'ember-emberfire-find-query/mixins/find-query';
import _ from "lodash";

import {
  hash
} from 'rsvp';

export default Route.extend(FindQuery, {
  session: service(),
  state: service(),
  user: computed.alias("session.data.authenticated.user"),
  firebaseApp: service(),
  model() {
    var uid = this.get("user.uid");

    if (uid) {
      return hash({
        userInfo: this.store.query('user-info', {
          filter: {
            uid: uid
          }
        }),
        allContacts: this.store.query('contact', {
          filter: {
            uid: uid
          }
        }),
        lists: this.store.query('list', {
          filter: {
            uid: uid
          }
        }),
        listContactRels: this.store.query('list-contact-rel', {
          filter: {
            uid: uid
          }
        }),
      });
    } else {
      return hash({
        userInfo: [{}],
        allContacts: [{}],
        lists: [{}],
        listContactRels: [{}]
      });
    }

  },
  afterModel(model) {
    // if they don't have a user-info object associated with them, create it.
    if (model.userInfo.toArray().length === 0) {
      this.store.createRecord('user-info', {
          plan: "basic",
          uid: this.get('user.uid')
        })
        .save();
    }
  }
});
