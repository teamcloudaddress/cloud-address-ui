/* eslint-disable ember/no-global-jquery */
import Component from '@ember/component';
import {
  computed
} from "@ember/object";
import {
  inject as service
} from '@ember/service';
import _ from "lodash";

export default Component.extend({
  state: service(),
  list: service(),
  toasts: service(),

  didInsertElement() {
    $("div")
      .not(".list-actions-dropdown")
      .click(function () {
        $(".list-actions-dropdown").slideUp();
      })
  },
  userPlan: computed("model.userInfo.plan", function () {
    let userInfo = _.first(this.get('model.userInfo').toArray());

    if (userInfo && userInfo.plan) {
      return userInfo.plan
    } else {
      return "basic";
    }
  }),
  userFirstLetter: computed('user.email', function () {
    return this.get('user.email')[0].toUpperCase();
  }),
  isPro: computed("userPlan", function () {
    return this.get('userPlan') === "pro";
  }),
  lists: computed("model.lists", "list.all", function () {
    if (this.get("list.all")) {
      return this.get("list.all");
    } else {
      return this.get("model.lists");
    }
  }),
  selectedList: computed.alias("state.selectedList"),
  selectedListId: computed("selectedList.id", function () {
    return this.get("selectedList.id");
  }),
  selectedListName: computed("selectedList.name", function () {
    return this.get("selectedList.name");
  }),
  listLength: computed("lists", function () {
    return this.get("lists").length;
  }),

  actions: {
    addList() {
      this.get('list').create(this.get('store'));
      this.get('toasts').success('List successfully created.');
      if ($(document).width() < 1000) {
        this.send('hideSideNav');
      }
    },
    deleteList(listId) {
      this.get('deleteList')(listId);
      this.get('toasts').success('List successfully deleted.');
    },
    showListActionsDropdown(listId) {
      if ($("#list-actions-dropdown-" + listId).css('display') !== "block") {
        $("#list-actions-dropdown-" + listId).slideDown();
      }
    },
    logout() {
      this.get("logout")();
    },
    sendHome() {
      this.get("sendHome")();
    },
    viewAll() {
      if ($(document).width() < 1000) {
        this.send('hideSideNav');
      }
      this.set('state.selectedList', null);
      this.get('sendToIndexRoute')();
    },
    viewList(list) {
      if ($(document).width() < 1000) {
        this.send('hideSideNav');
      }
      this.set("state.selectedList", list);
      this.get('sendToIndexRoute')();
    },
    hideSideNav() {
      $("#side-nav").slideUp(300);
    }
  }
});
