import Component from '@ember/component';

export default Component.extend({
    actions: {
        confirm() {
            this.get('confirm')();
        },
        cancel() {
            this.get("cancel")();
        }
    }
});
