/* eslint-disable ember/jquery-ember-run */
/* eslint-disable ember/no-global-jquery */
import Component from '@ember/component';
import {
  inject as service
} from '@ember/service';

export default Component.extend({
  analytics: service(),
  session: service(),

  didInsertElement() {
    if (!this.get('session.isAuthenticated')) {
      this.get('goToWelcomePage')();
    }

    $("div")
      .click(function (e) {

        if ($(e.target).hasClass("no-hide")) {
          e.preventDefault();

          return;
        }

        $(".contact-actions-dropdown").css('display', 'none');
      })

    $("#modal-gauze").click(e => {
      if ($(e.target).hasClass("no-hide")) {
        e.preventDefault();
        return;
      }

      $("#modal-gauze").fadeOut();
    })

    $("#import-contacts-modal-gauze").click(e => {
      if ($(e.target).hasClass("no-hide")) {
        e.preventDefault();

        return;
      }

      $("#import-contacts-modal-gauze").fadeOut();
    })


    window.intercomSettings = {
      app_id: "p7hicx9y",
      name: this.get('user.displayName'),
      email: this.get('user.email'),
      created_at: this.get('user.metadata.creationTime')
    };
    (function () {
      var w = window;
      var ic = w.Intercom;
      if (typeof ic === "function") {
        ic('reattach_activator');
        ic('update', w.intercomSettings);
      } else {
        var d = document;
        var i = function () {
          i.c(arguments);
        };
        i.q = [];
        i.c = function (args) {
          i.q.push(args);
        };
        w.Intercom = i;
        var l = function () {
          var s = d.createElement('script');
          s.type = 'text/javascript';
          s.async = true;
          s.src = 'https://widget.intercom.io/widget/p7hicx9y';
          var x = d.getElementsByTagName('script')[0];
          x.parentNode.insertBefore(s, x);
        };
        if (w.attachEvent) {
          w.attachEvent('onload', l);
        } else {
          w.addEventListener('load', l, false);
        }
      }
    })();

    this.get('analytics').init();

    setInterval(() => {
      // $("#contact-table-wrapper").css('height', $(window).height() - 82);
      $("#side-nav").css('height', $(window).height());
      if ($(window).width() > 1000) {
        $("#side-nav ").fadeIn(200);
      }

    }, 500);
  }
});
