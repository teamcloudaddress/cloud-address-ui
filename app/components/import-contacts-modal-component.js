import Component from '@ember/component';
import parseCSV from "../utils/parse-csv";
import getContactCount from "../utils/determine-contacts-count-in-upload";
import { computed } from "@ember/object";

export default Component.extend({
    browserIsCompatible: computed(function () {
        var isCompatible = false;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            isCompatible = true;
        }
        return isCompatible;
    }),

    contactsStagedForUpload: null,
    numberOfContactsUploaded: null,

    actions: {
        reset() {
            this.set("numberOfContactsUploaded", 0);
            this.set('contactsStagedForUpload', null);
        },
        upload(evt) {
            let self = this;
            var data = null;
            var file = evt.target.files[0];
            var reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function (event) {
                var csvData = event.target.result;

                data = parseCSV(csvData);
                if (data && data.length > 0) {
                    self.set('numberOfContactsUploaded', getContactCount(data));
                    self.set('contactsStagedForUpload', data);
                } else {
                    alert('No data to import!');
                }
            };
            reader.onerror = function () {
                alert('Unable to read ' + file.fileName);
            };

        },
        confirm() {
            this.get('confirm')(this.get('contactsStagedForUpload'));

            // this lets the modal fade out before reseting everything
            setTimeout(() => {
                this.send('reset');
            }, 1000);
        },
        cancel() {
            this.get("cancel")();

            // this lets the modal fade out before reseting everything
            setTimeout(() => {
                this.send('reset');
            }, 1000);
        }
    }
});
