import Component from '@ember/component';
import EmberObject, {
  computed
} from "@ember/object";
import {
  A
} from "@ember/array";
import _ from "lodash";

export default Component.extend({
  selected: EmberObject.create({
    contacts: A([])
  }),
  currentContacts: computed('contacts', 'selected.contacts.[]', function () {

    if (this.get('contacts').length > 0) {

      return _.map(this.get("contacts").toArray(), c => {
        if (_.isFunction(c.set)) {

          if (_.includes(this.get('selected.contacts'), c.id)) {

            c.set('selected', true);
          } else {
            c.set('selected', false);
          }
        }

        return c;
      });
    } else {
      return [];
    }
  }),
  actions: {
    addContacts() {
      if (this.get('selected.contacts').length > 0) {
        this.get('addContacts')(this.get('selected.contacts'));
        this.set('selected.contacts', A([]));
      }
    },
    toggleContact(contactId) {

      let isSelected = _.includes(this.get('selected.contacts'), contactId);

      if (isSelected) {
        let filteredContacts = _.filter(this.get('selected.contacts'), contact => {
          return contact !== contactId;
        });

        this.set('selected.contacts', filteredContacts);
      } else {
        this.get("selected.contacts").pushObject(contactId);
      }
    },
    closeModal() {
      // eslint-disable-next-line ember/no-global-jquery
      $("#modal-gauze").fadeOut();

      this.set('selected.contacts', A([]));
    }
  }
});
