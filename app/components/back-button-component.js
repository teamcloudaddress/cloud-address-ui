import Component from '@ember/component';

export default Component.extend({
    actions: {
        goBack() {
            this.get('onBack')();
        }
    }
});
