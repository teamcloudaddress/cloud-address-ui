/* eslint-disable ember/no-global-jquery */
import Component from '@ember/component';
import {
  computed
} from "@ember/object";
import {
  inject as service
} from '@ember/service';
import _ from "lodash";

export default Component.extend({
  state: service(),
  list: service(),
  toasts: service(),

  headerEditState: false,
  headerEditValue: computed("displayHeader", function () {
    return this.get('displayHeader');
  }),
  selectedList: computed.alias("state.selectedList"),
  headerEditable: computed("selectedList.name", "header", function () {
    return this.get('selectedList') && !this.get('header');
  }),
  displayHeader: computed("selectedList", "header", "headerEditValue", function () {
    if (this.get('header')) {
      return this.get('header').toUpperCase();
    } else if (this.get('selectedList')) {
      return this.get("state.selectedList.name").toUpperCase();
    } else {
      return "ALL CONTACTS";
    }
  }),
  listCount: computed.alias('selectedList.count'),
  successMessage: computed.alias('toasts.successMessage'),
  saveSuccessToast: computed('toasts.showSaveSuccessToast', function () {

    if (this.get("toasts.showSaveSuccessToast")) {
      $("#changes-success-toast").slideDown().delay(2000).slideUp();
    }

    return "";
  }),

  colorPickerColor: null,

  messageSuccessToast: computed('toasts.showMessageSuccessToast', function () {

    if (this.get("toasts.showMessageSuccessToast")) {
      $("#message-success-toast").slideDown().delay(2000).slideUp();
    }

    return "";
  }),
  didReceiveAttrs() {
    let color = this.get('selectedList.color');

    if (color === "default" || _.isNull(color) || _.isUndefined(color)) {
      this.set('colorPickerColor', "#ccc");
    } else {
      this.set('colorPickerColor', `${color}`);
    }
  },
  actions: {
    colorChanged(color) {
      this.get('list').updateColor(this.get('store'), this.get('state.selectedList.id'), color);
    },
    onImport() {
      this.get("onImport")();
    },
    onExport() {
      this.get("onExport")();
    },
    addContact() {
      this.get("addContact")();
    },
    createContact() {
      this.get("createContact")();
    },
    updateHeaderEditValue(val, event) {
      if (event.keyCode === 13) {
        this.send("toggleOffEditListNameState");
      } else {
        this.set('headerEditValue', val);
      }
    },
    toggleOnEditListNameState() {
      this.set('headerEditState', true);
      setTimeout(() => {
        $("#edit-list-name-field").select();
      }, 50);
    },
    toggleOffEditListNameState() {
      if (this.get('headerEditValue').trim().length > 0) {
        this.get('list').rename(this.get('store'), this.get('state.selectedList.id'), this.get('headerEditValue'));
        this.set('headerEditState', false);
        this.get("toasts").successfullySaved();
      }
    },
    showSideNav() {
      // $("#side-nav").addClass("expanded");
      $("#side-nav").slideDown(200);
    }
  }
});
