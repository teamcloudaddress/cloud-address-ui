import Component from '@ember/component';

export default Component.extend({
    actions: {
        onChange() {
            this.get('onChange')();
        }
    }
});
