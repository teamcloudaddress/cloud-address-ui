import { helper } from '@ember/component/helper';
import _ from "lodash";

export function checkIfContactIsAddedToList([relationships, listId, contactId]) {
  return _.find(relationships.toArray(), val => {
    return val.listId === listId && val.contactId === contactId;
  });
}

export default helper(checkIfContactIsAddedToList);
