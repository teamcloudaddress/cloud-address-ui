import { helper } from '@ember/component/helper';
import _ from 'lodash';

export function lodash([method, ...params]) {
  return _[method](params);
}

export default helper(lodash);
