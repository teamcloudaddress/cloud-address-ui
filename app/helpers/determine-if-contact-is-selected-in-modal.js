import { helper } from '@ember/component/helper';
import _ from "lodash";

export function determineIfContactIsSelectedInModal([selectedIds, contactId, div]) {
  return _.includes(selectedIds, contactId);
}

export default helper(determineIfContactIsSelectedInModal);
