import { helper } from '@ember/component/helper';
import _ from "lodash";

export function formatTimestamp([timestamp]) {
  return moment(_.toNumber(timestamp)).fromNow();
}

export default helper(formatTimestamp);
