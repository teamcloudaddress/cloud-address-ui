import {
  helper
} from '@ember/component/helper';
import _ from "lodash";
import {
  htmlSafe
} from '@ember/string';


export function getListColor([color]) {
  if (color === "default" || _.isNull(color) || _.isUndefined(color)) {
    return htmlSafe("border-color: #ccc;");
  } else {
    return htmlSafe(`border-color: ${color};`);
  }
}

export default helper(getListColor);
