module.exports = {
    globals: {
        server: true,
        'server': true,
        'Ember': true,
        '_': true,
        'ga': true,
        '$': true,
        'FB': true,
        'Ember.$': true,
        'moment': true,
        'Promise': true,
        'server': true,
        'setBreakpoint': true
    },
    root: true,
    parserOptions: {
        ecmaVersion: 2017,
        sourceType: 'module'
    },
    plugins: [
        'ember'
    ],
    extends: [
        'eslint:recommended',
        'plugin:ember/recommended'
    ],
    env: {
        browser: true
    },
    rules: {
    },
    overrides: [
    // node files
        {
            files: [
                '.eslintrc.js',
                '.template-lintrc.js',
                'ember-cli-build.js',
                'testem.js',
                'blueprints/*/index.js',
                'config/**/*.js',
                'lib/*/index.js',
                'server/**/*.js'
            ],
            parserOptions: {
                sourceType: 'script',
                ecmaVersion: 2015
            },
            env: {
                browser: false,
                node: true
            },
            plugins: ['node'],
            rules: Object.assign({}, require('eslint-plugin-node').configs.recommended.rules, {
                'indent': [
                    'error',
                    4
                ],
                // There is no support for string interpolation tick-marks (i.e. ``):
                'quotes': [
                    'error',
                    'single'
                ],
                'semi': [
                    'error',
                    'always'
                ],
                'no-useless-escape': 'off'
            })
        }
    ]
};
